
/*
 * GET home page.
 */

var config = require('../config/config.js');

exports.index = function(req, res){
    res.render('index', { title: 'Main of ' + config.projectName });
};

exports.admin = function (req, res) {
    if (req.user) {
        res.render('admin/index', {
            title  : 'Admin Web Page',
            layout : 'layout'
        });
    } else {
        res.render(
            'admin/login', {
                title : 'login'
        });
    }
}